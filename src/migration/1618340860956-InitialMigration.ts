import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1618340860956 implements MigrationInterface {
    name = 'InitialMigration1618340860956'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `allocations_results` (`id` int NOT NULL AUTO_INCREMENT, `currency` varchar(45) NOT NULL, `allocation_name` varchar(45) NOT NULL, `execution_id` int NOT NULL, `value_percent` float NOT NULL, `add_date` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `investment_universe_status` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(45) NOT NULL, `status` varchar(45) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `stock_quotations` (`id` int NOT NULL AUTO_INCREMENT, `currency_pair` varchar(45) NOT NULL, `open` decimal(20,8) NOT NULL, `high` decimal(20,8) NOT NULL, `low` decimal(20,8) NOT NULL, `close` decimal(20,8) NOT NULL, `vwap` decimal(20,8) NOT NULL, `volume` decimal(20,8) NOT NULL, `count` decimal(20,8) NOT NULL, `add_date` int NOT NULL, `time` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `strategies_results` (`id` int NOT NULL AUTO_INCREMENT, `strategy_name` varchar(45) NOT NULL, `currency_pair` varchar(45) NOT NULL, `signal` varchar(45) NOT NULL, `add_date` int NOT NULL, `execution_id` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `transactions` (`id` int NOT NULL AUTO_INCREMENT, `currency_pair` varchar(45) NOT NULL, `order_type` varchar(45) NOT NULL, `price` int NOT NULL, `add_date` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `SEQUENCE` (`SEQ_NAME` varchar(50) NOT NULL, `SEQ_COUNT` decimal(38,0) DEFAULT NULL, PRIMARY KEY (`SEQ_NAME`)) ENGINE=InnoDB");
        await queryRunner.query("INSERT INTO `cryptobot`.`SEQUENCE`(`SEQ_NAME`,`SEQ_COUNT`) VALUES('SEQ_GEN',0)");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('ZEC_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('XRP_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('XMR_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('XLM_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('REP_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('LTC_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('GNO_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('ETH_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('ETC_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('EOS_EUR', 'TRADABLE')");
        await queryRunner.query("INSERT INTO `cryptobot`.`investment_universe_status`(`name`,`status`)VALUES('BTC_EUR', 'TRADABLE')");

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `strategies_results`");
        await queryRunner.query("DROP TABLE `stock_quotations`");
        await queryRunner.query("DROP TABLE `investment_universe_status`");
        await queryRunner.query("DROP TABLE `allocations_results`");
    }

}
