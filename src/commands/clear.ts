import { createConnection } from "typeorm";
import { AllocationsResults } from "../entity/allocations_results";
import { InvestmentUniverseStatus } from "../entity/investment_universe_status";
import { StockQuotations } from "../entity/stock_quotations";
import { StrategiesResults } from "../entity/strategies_results";
import { User } from "../entity/user";
import { Transactions } from "../entity/transactions";

const entities = [
    AllocationsResults,
    InvestmentUniverseStatus,
    StockQuotations,
    StrategiesResults,
    User,
    Transactions,
];

async function clearAll() {
    const connection = await createConnection();
    for (const entity of entities) {
        const repository = await connection.getRepository(entity.name);
        await repository.clear();
    }
}

clearAll()
    .then(() => {
        process.exit(0);
    });