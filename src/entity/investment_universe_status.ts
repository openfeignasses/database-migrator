import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('investment_universe_status')
export class InvestmentUniverseStatus {
    @PrimaryColumn()
    id: number;

    @Column({length: 45})
    name: string;

    @Column({length: 45})
    status: string;
}