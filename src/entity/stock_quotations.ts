import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('stock_quotations')
export class StockQuotations {
    @PrimaryColumn()
    id: number;

    @Column({name: 'currency_pair', length: 45, nullable: false})
    currencyPair: string;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    open: number;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    high: number;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    low: number;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    close: number;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    vwap: number;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    volume: number;

    @Column({ type: 'decimal', precision: 20, scale: 8, nullable: false})
    count: number;

    @Column({name: 'add_date', type: 'int'})
    addDate: number;

    @Column()
    time: Date;
}