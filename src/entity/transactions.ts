import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('transactions')
export class Transactions {
    @PrimaryColumn()
    id: number;

    @Column({name: 'currency_pair', length: 45})
    currencyPair: string;
    
    @Column({name: 'order_type', length: 45})
    orderType: string;

    @Column({type: 'int'})
    price: number;

    @Column({name: 'add_date', type: 'int'})
    addDate: number;
}