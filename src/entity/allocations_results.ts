import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity('allocations_results')
export class AllocationsResults {
    @PrimaryColumn()
    id: number;

    @Column({length: 45})
    currency: string;

    @Column({name: 'allocation_name', length: 45})
    allocationName: string;

    @Column({name: 'execution_id', type: 'int'})
    executionId: number;

    @Column({name: 'value_percent', type: 'float'})
    valuePercent: number;

    @Column({name: 'add_date', type: 'int'})
    addDate: number;
}