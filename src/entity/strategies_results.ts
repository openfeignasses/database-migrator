import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('strategies_results')
export class StrategiesResults {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({name: 'strategy_name', length: 45})
    strategyName: string;

    @Column({name: 'currency_pair', length: 45})
    currencyPair: string;

    @Column({length: 45})
    signal: string;

    @Column({name: 'add_date', type: 'int'})
    addDate: number;

    @Column({name: 'execution_id', type: 'int'})
    executionId: number;
}