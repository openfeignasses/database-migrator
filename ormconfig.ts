import * as dotenv from 'dotenv';

dotenv.config();

export default {
   type: 'mysql',
   host: process.env.DATABASE_HOST,
   port: process.env.DATABASE_PORT,
   username: process.env.DATABASE_USER,
   password: process.env.DATABASE_PASSWORD,
   database: process.env.DATABASE_DB || 'cryptobot',
   entities: ['src/entity/*{.ts,.js}'],
   migrations: ['src/migration/*{.ts,.js}'],
   cli: {
      migrationsDir: 'src/migration'
   }
}