FROM node
WORKDIR /app
ADD package*.json ./
RUN npm install
ADD . .
RUN chmod +x wait-for-it.sh
CMD sh -c "npm run typeorm migration:run"
