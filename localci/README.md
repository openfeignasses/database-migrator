# commons-schema

Schema SQL pour la base de données. La même base de données est utilisée pour tous les modules.

# Configuration

- docker
- MySQL Workbench

### Instance locale

Démarrer une instance locale:
`./localci/start-db.sh`

Vider la base et redémarrer:
`./localci/start-db.sh`

Arrêter la base:
`./localci/stop-db.sh`

### Edition du schéma

1. Démarrer la base
2. S'y connecter via MySQL Workbench
3. Importer le schéma courant
4. Faire des modifications
5. Exporter les commandes de création CREATE TABLE
6. Si une table a été ajoutée: Ajouter 'IF NOT EXISTS' pour permettre l'idempotence
7. Si une table a été ajoutée: Ajouter la commande TRUNCATE correspondante
8. Push et laisser la pipeline relancer les tests de tous les modules

### Migration de données

Il se peut qu'une mise à jour du schéma soie incompatible avec le schéma courant.  
La pipeline échouera alors à "test-data-migration".   

Pour migrer les données:
1. Se connecter au compte AWS
2. Télécharger le fichier "dump-production-data.sql" depuis S3
3. Faire les modifications nécessaires
4. Uploader le fichier modifié
5. Cliquer sur le bouton "Retry" du job "test-data-migration"

### Réimporter un dump créé avec MySQL Workbench

*Depuis dossier racines des repos du bot*  
`docker run -v $(pwd):/feignasses -it --rm mysql:5.7.26 mysql -h172.17.0.2 -uroot -pA9GHJJH79889dhdk890`  
`mysql> use cryptobot`  
`source /feignasses/dump-debug-data-provider.sql`