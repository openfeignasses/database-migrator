CREATE TABLE IF NOT EXISTS `stock_quotations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_pair` varchar(45) NOT NULL,
  `open` DECIMAL(20, 8) NOT NULL,
  `high` DECIMAL(20, 8) NOT NULL,
  `low` DECIMAL(20, 8) NOT NULL,
  `close` DECIMAL(20, 8) NOT NULL,
  `vwap` DECIMAL(20, 8) NOT NULL,
  `volume` DECIMAL(20, 8) NOT NULL,
  `count` DECIMAL(20, 8) NOT NULL,
  `add_date` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `investment_universe_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'TRADABLE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `allocations_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(45) NOT NULL,
  `allocation_name` varchar(45) NOT NULL,
  `execution_id` int(11) NOT NULL,
  `value_percent` float NOT NULL,
  `add_date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `strategies_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strategy_name` varchar(45) NOT NULL,
  `currency_pair` varchar(45) NOT NULL,
  `signal` varchar(45) NOT NULL,
  `add_date` int(11) NOT NULL,
  `execution_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- For local environment purpose. To make sure tables are emptied if they exist.
TRUNCATE TABLE `cryptobot`.`stock_quotations`;
TRUNCATE TABLE `cryptobot`.`investment_universe_status`;
TRUNCATE TABLE `cryptobot`.`allocations_results`;
TRUNCATE TABLE `cryptobot`.`strategies_results`;
TRUNCATE TABLE `cryptobot`.`SEQUENCE`;

