# database-migrator

This Typescript module creates the database schema, and imports data fixtures. It is based on TypeORM.

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

# Run migrations

```bash
npm run typeorm migration:run
```

# Insert fixtures

```bash
npm run fixtures -- -c ormconfig.ts src/fixtures/allocations/dryrun
```

You can change `src/fixtures/allocations/dryrun` by another fixtures path.